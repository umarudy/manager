import React, { Component } from 'react';
// import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyD0hgF9zEWyZ9W7vfw-wqgZF8yCLsh9iWU',
            authDomain: 'manager-99c52.firebaseapp.com',
            databaseURL: 'https://manager-99c52.firebaseio.com',
            projectId: 'manager-99c52',
            storageBucket: 'manager-99c52.appspot.com',
            messagingSenderId: '247309920854'
          };
          firebase.initializeApp(config);
    } 

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (
            <Provider store={store}>
               <Router />
            </Provider>
        );
    }
}

export default App;
